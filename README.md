
[![Docker Stars](https://img.shields.io/docker/stars/nettematic/debian.svg?style=flat)](https://hub.docker.com/r/nettematic/debian/)
[![Docker Pulls](https://img.shields.io/docker/pulls/nettematic/debian.svg?style=flat)](https://hub.docker.com/r/nettematic/debian/)

# Debian
Základní image, který je založen na debianu. Obsahuje speciální varianty pro různé verze debianu (Jessie, Stretch)


